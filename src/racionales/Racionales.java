package racionales;

/**
 * Clase encargada de representar a los numeros racionales.
 * @author luis
 */
public class Racionales {
    
    // Variable que representa al numerador.
    int p;
    // Variable que representa al denominador.
    int q;
    
    /*
     * Constructor por default (Sin parametros).
    */
    Racionales(){
        this.p = 1;
        this.q = 1;
    }
    
    /*
     * Constructor por parametros.
    */
    Racionales(int p, int q){
        this.p = p;
        this.q = q;
    }
    
    /**
     * Metodo que asigna un nuevo valor al numerador.
     * @param p El nuevo valor del numerador.
     */
    private void setNumerador(int p){
        this.p = p;
    }
    
    /**
     * Metodo que asigna un nuevo valor al denominador.
     * @param q El nuevo valor del denominador.
     */
    private void setDenominador(int q){
        this.q = q;
    }
    
    /**
     * Metodo que regresa el valor del numerador.
     * @return El valor del numerador.
     */
    private int getNumerador(){
        return this.p;
    }
    
    /**
     * Metodo que regresa el valor del denominador.
     * @return El valor del denominador.
     */
    private int getDenominador(){
        return this.q;
    }
    
    /**
     * Metodo que imprime el numero racional.
     */
    void show(){
        System.out.println(getNumerador() + "/" + getDenominador());
    }
    
    /**
     * Metodo que realiza la multiplicacion de dos numeros racionales.
     * @param r El numero racional con el que multiplicaremos.
     * @return El resultado de la multiplicacion.
     */
    Racionales multiplicaRacionales(Racionales r){
        int p = 0;
        int q = 1;
        
        p = getNumerador() * r.getNumerador();
        q = getDenominador() * r.getDenominador();
        
        return (new Racionales(p, q));
    }
    Racionales divideRacionales(Racionales r){
        int p=0;
        int q=1;
        
        p= getNumerador ()* r.getDenominador();
        q= getDenominador()* r.getNumerador();
        return (new Racionales(p,q));
    }
    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Creamos dos objetos que representaran a dos numeros racionales.
        Racionales r1 = new Racionales(1, 2);
        Racionales r2 = new Racionales(2, 3);
        
        // Declaramos un objeto (OJO!!!, aun no creamos el objeto).
        Racionales r3;
        Racionales r4;
        
        // Imprimimos r1:
        System.out.print("R1 es: ");
        r1.show();
        
        // Imprimimos r2:
        System.out.print("R2 es: ");
        r2.show();
        
        // En r3 guardaremos el resultado.
        // MULTIPLICACION:
        r3 = r1.multiplicaRacionales(r2);
        r4= r1.divideRacionales(r2);
        
        // Imprimimos el resultado.
        System.out.print("El resultado de la multiplicacion es: ");
        r3.show();
        System.out.print("El resultado de la division es: ");
        r4.show();
    }
    
}

